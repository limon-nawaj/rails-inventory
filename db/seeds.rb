# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


OrderItem.create(product_id: 1, order_id: 1, unit_price: 0.15e2, quantity: 1, total_price: 0.15e2, created_at: "2018-02-16 06:59:23", updated_at: "2018-02-16 06:59:23")
OrderItem.create(product_id: 2, order_id: 1, unit_price: 0.15e2, quantity: 1, total_price: 0.15e2, created_at: "2018-01-07 06:59:23", updated_at: "2018-02-07 06:59:23")
OrderItem.create(product_id: 1, order_id: 1, unit_price: 0.15e2, quantity: 1, total_price: 0.15e2, created_at: "2018-01-09 06:59:26", updated_at: "2018-02-09 06:59:26") 


# Product.create(category_id: 1, unit_id: 1, brand_id: 2, name: "Item 1", unit_price: 15.0, quantity: 200.0)
# Product.create(category_id: 2, unit_id: 4, brand_id: 3, name: "Item 2", unit_price: 15.0, quantity: 200.0)
# Product.create(category_id: 3, unit_id: 5, brand_id: 2, name: "Item 3", unit_price: 15.0, quantity: 200.0)
# Product.create(category_id: 1, unit_id: 1, brand_id: 3, name: "Item 4", unit_price: 15.0, quantity: 200.0)
# Product.create(category_id: 2, unit_id: 1, brand_id: 2, name: "Project 1", unit_price: 15.0, quantity: 200.0)
# Product.create(category_id: 3, unit_id: 1, brand_id: 3, name: "Project 2", unit_price: 15.0, quantity: 200.0)
# Product.create(category_id: 2, unit_id: 3, brand_id: 2, name: "Project 3", unit_price: 15.0, quantity: 200.0)
# Product.create(category_id: 1, unit_id: 2, brand_id: 3, name: "Mobile 1", unit_price: 15.0, quantity: 200.0)
# Product.create(category_id: 3, unit_id: 3, brand_id: 2, name: "Mobile 2", unit_price: 15.0, quantity: 200.0)
# Product.create(category_id: 1, unit_id: 1, brand_id: 2, name: "Mobile 3", unit_price: 15.0, quantity: 200.0)
# Product.create(category_id: 2, unit_id: 4, brand_id: 3, name: "Mobile 4", unit_price: 15.0, quantity: 200.0)

