class CreateProductDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :product_details do |t|
      t.integer :product_id
      t.float :purchase_price, default: 0
      t.float :previous_quantity, default: 0 
      t.float :quantity, default: 0

      t.timestamps
    end
  end
end
