class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string  :product_code
      t.integer :category_id
      t.integer :unit_id
      t.integer :brand_id
      t.string  :name
      t.integer :picture
      t.float   :unit_price, default: 0
      t.float   :quantity, default: 0
      t.float   :notify_limit, default: 0
      t.string :slug

      t.timestamps
    end
  end
end
