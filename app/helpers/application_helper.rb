module ApplicationHelper

	def active_class(link_path)
	  current_page?(link_path) ? "active" : ""
	end

	def categories
	  @categories = Category.roots
	end
end
