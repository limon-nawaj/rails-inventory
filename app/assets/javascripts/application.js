// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
// = require turbolinks
//= require dataTables/jquery.dataTables
//= require moment
//= require bootstrap-datetimepicker
//= require_tree .






var ready;
ready = function() {
  $('#products-table').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": $('#products-table').data('source'),
    "pagingType": "full_numbers",
    "columns": [
      {"data": "name"},
      {"data": "category"},
      {"data": "unit"},
      {"data": "brand"},
      {"data": "quantity"},
      {"data": "unit_price"},
      {"data": "action"},
    ]
  });
};

var all_search;
all_search = function() {
  $("#all-results").live("click", function() {
    $.getScript(this.href);
    return false;
  });
  $("#products_search input").keyup(function() {
    $.get($("#products_search").attr("action"), $("#products_search").serialize(), null, "script");
    return false;
  });
};

var search_by_category;
search_by_category = function() {
  $("#products-by-category").live("click", function() {
    $.getScript(this.href);
    return false;
  });
  $("#search-products-by-category input").keyup(function() {
    $.get($("#search-products-by-category").attr("action"), $("#search-products-by-category").serialize(), null, "script");
    return false;
  });
};

var sales_datepicker;
sales_datepicker = function () {
    $('#sales_start_date').datepicker({
      format: 'dd-mm-yyyy'
    });
    $('#sales_end_date').datepicker({
      format: 'dd-mm-yyyy'
    });
};

$(document).ready(ready)
$(document).on('page:load', ready)
$(document).ready(all_search)
$(document).on('page:load', all_search)
$(document).ready(search_by_category)
$(document).on('page:load', search_by_category)
$(document).ready(sales_datepicker)
$(document).on('page:load', sales_datepicker)