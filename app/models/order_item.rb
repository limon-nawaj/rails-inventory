class OrderItem < ApplicationRecord
  belongs_to :product
  belongs_to :order

  validates :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :unit_price, numericality: true, presence: true
  validate :product_present
  # validate :order_present

  before_save :finalize


  scope :today, lambda {where(created_at: Date.today.beginning_of_day..Date.today.end_of_day)}
  scope :current_week, lambda {where(created_at: Date.today.beginning_of_week.beginning_of_day..Date.today.end_of_week.end_of_day)}
  scope :last_week, lambda {where(created_at: Date.today.last_week.beginning_of_week.beginning_of_day..Date.today.last_week.end_of_week.end_of_day)}

  scope :current_month, lambda {where(created_at: Date.today.beginning_of_month.beginning_of_day..Date.today.end_of_month.end_of_day)}
  scope :last_month, lambda {where(created_at: Date.today.last_month.beginning_of_month.beginning_of_day..Date.today.last_month.end_of_month.end_of_day)}

  # scope :current_month_sales_rate, lambda {OrderItem.current_month.count*100/Product.sum(:quantity) if Product.sum(:quantity)>0}
  # scope :weekly_sales_rate_of_current_month, lambda {OrderItem.current_week.count*100/OrderItem.current_month.count if OrderItem.current_month.count>0}

  

  def self.current_month_sales_rate
    if Product.sum(:quantity)>0
      self.current_month.count*100/Product.sum(:quantity) 
    else
      0
    end
  end

  def self.weekly_sales_rate_of_current_month
    if OrderItem.current_month.count>0
      self.current_week.count*100/OrderItem.current_month.count 
    else
      0
    end
  end


  # @weekly_top_sales = OrderItem.current_week.group(:product_id).order('sum_quantity desc').sum(:quantity)
  #   @monthly_top_sales = OrderItem.current_month.group(:product_id).order('sum_quantity desc').sum(:quantity)


  def unit_price
    if persisted?
      self[:unit_price]
    else
      product.unit_price
    end
  end

  def total_price
    unit_price * quantity
  end

	private
	  def product_present
	    if product.nil?
	      errors.add(:product, "is not valid or is not active.")
	    end
	  end

	  # def order_present
	  #   if order.nil?
	  #     errors.add(:order, "is not a valid order.")
	  #   end
	  # end

	  def finalize
	    self[:unit_price] = unit_price
	    self[:total_price] = quantity * self[:unit_price]
	  end
end
