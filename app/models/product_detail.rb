class ProductDetail < ApplicationRecord
	belongs_to :product
	
	validates :quantity, presence: true
	validates :purchase_price, presence: true, numericality: true
end
