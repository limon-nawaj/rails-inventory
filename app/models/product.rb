class Product < ApplicationRecord
	belongs_to :category
	belongs_to :unit
	belongs_to :brand
	has_many :product_details, :dependent => :destroy
	has_many :order_items

	before_destroy :destroy_details

	extend FriendlyId
	friendly_id :name, use: :slugged

	validates :product_code, presence: true, uniqueness: true
	validates :name, presence: true
	validates :category_id, presence: true
	validates :unit_id, presence: true
	validates :brand_id, presence: true
	validates :unit_price, presence: true, numericality: true

	def self.search(search)
		where('name LIKE ? ', "%#{search}%")
	end

	def self.search_by_category(category, search)
		category.products.where('name LIKE ? ', "%#{search}%")
	end

	private
		def destroy_details
			self.product_details.delete_all
		end
end
