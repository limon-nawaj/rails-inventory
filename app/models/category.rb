class Category < ApplicationRecord
	acts_as_nested_set
	extend FriendlyId
	friendly_id :title, use: :slugged

  	has_many :products
end
