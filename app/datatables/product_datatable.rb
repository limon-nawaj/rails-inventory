class ProductDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :link_to, :edit_product_path, :product_details_path, :fa_icon, :product_path
  


  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      name: { source: "Product.name", cond: :like, searchable: true, orderable: true },
      category: { source: "Category.title", cond: :like, searchable: true, orderable: false },
      unit: { source: "Unit.name", cond: :like, searchable: true, orderable: false },
      brand: { source: "Brand.name", cond: :like, searchable: true, orderable: false },
      quantity: { source: "Product.quantity", cond: :like, searchable: false, orderable: false },
      unit_price: { source: "Product.unit_price", cond: :like, searchable: false, orderable: false },
      action: { source: "Action", cond: :null_value, searchable: false, orderable: false },
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        name: record.name,
        category: record.category ? record.category.title : nil ,
        unit: record.unit ? record.unit.name : nil,
        brand: record.brand ? record.brand.name : nil,
        quantity: record.quantity,
        unit_price: record.unit_price,
        action: link_to(fa_icon('trash-o lg'), product_path(record), :method => :delete, data: {:confirm => 'Are you sure?'}) + " " + link_to(fa_icon('edit lg'), edit_product_path(record)) + " " + link_to(fa_icon('plus-circle lg'), product_details_path(record)),
        
      }
    end
  end

  private

  def get_raw_records
    Product.includes(:category, :unit, :brand).references(:category, :unit, :brand)
  end

  

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
