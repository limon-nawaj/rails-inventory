class DetailsController < ApplicationController
	layout 'dashboard'
	before_action :set_product
	def index
		@product_details = @product.product_details
	end

	def new
		@detail = ProductDetail.new
	end

	def create
		@detail = ProductDetail.new(detail_params)
		@detail.previous_quantity = @product.quantity
		if @detail.save
			@product.quantity += @detail.quantity
			@product.save
			flash[:success] = "New product has been added!"
			redirect_to product_details_path
		else
			render "new"
		end
	end

	def destroy
		@detail = ProductDetail.find(params[:id])
		@product.quantity = @product.quantity - @detail.quantity
		@product.save
		@detail.destroy
		flash[:danger] = "Item has been deleted successfully!"
		redirect_to product_details_path
	end

	private
		def detail_params
			params.require(:product_detail).permit(:product_id, :purchase_price, :previous_quantity, :quantity)
		end

		def set_product
			@product = Product.friendly.find(params[:product_id])
		end
end
