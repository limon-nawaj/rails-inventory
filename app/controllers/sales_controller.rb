class SalesController < ApplicationController
	layout 'dashboard'

	def index
		# search = params[:search] if params[:search].present?
		start_date 	= params[:start_date].to_date.beginning_of_day if params[:start_date].present?
		end_date = params[:end_date].to_date.end_of_day if params[:end_date].present?
		
		if start_date.present? && end_date.present?
			@sales = OrderItem.where(created_at: start_date..end_date).order(created_at: :desc).paginate(page: params[:page], per_page: 10)
		else
			@sales = OrderItem.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
		end
		
	end


end
