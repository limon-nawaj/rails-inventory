class StaticController < ApplicationController
	layout 'dashboard', except: [:home, :products_by_category]
	
	def home
		@products = Product.all
		@order_item = current_order.order_items.new

		if params[:search].present?
			@products = Product.search(params[:search])
		end
	end

	def dashboard
		@weekly_sales = OrderItem.current_week.order(created_at: :desc)
		@monthly_sales = OrderItem.current_month.order(created_at: :desc)
		@today_sales = OrderItem.today.order(created_at: :desc)


		@daily_top_sales = OrderItem.today.group(:product_id).order('sum_quantity desc').sum(:quantity)
		@weekly_top_sales = OrderItem.current_week.group(:product_id).order('sum_quantity desc').sum(:quantity)
		@monthly_top_sales = OrderItem.current_month.group(:product_id).order('sum_quantity desc').sum(:quantity)

	end

	def setting
		@unit = Unit.new
		@brand = Brand.new
	end

	def products_by_category
		@order_item = current_order.order_items.new
		@category = Category.where(slug: params[:category]).first
		@products = @category.products
		if params[:search].present?
			@products = Product.search_by_category(@category, params[:search])
		end
	end
end

