class CategoriesController < ApplicationController
	layout 'dashboard', except: [:show]
	before_action :set_category, only: [:edit, :update, :show, :destroy]

	def index
		@categories = Category.roots
	end

	def new
		@category = Category.new
	end

	def create
		@category = Category.new(category_params)
		if @category.save
			flash[:success] = "Category has saved!"
			redirect_to categories_path			
		else
			render 'new'
		end
	end

	def show
		@order_item = current_order.order_items.new
		@products = @category.products
		if params[:search].present?
			@products = Product.search_by_category(@category, params[:search])
		end
	end

	def edit
		
	end

	def update
		if @category.update_attributes(category_params)
			flash[:success] = "Category has been updated!"
			redirect_to categories_path
		else
			render 'edit'
		end
	end

	def destroy
		
	end


	private
		def category_params
			params.require(:category).permit(:title, :parent_id)
		end

		def set_category
			@category = Category.friendly.find(params[:id])
		end
end
