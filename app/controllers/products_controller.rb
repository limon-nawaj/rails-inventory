class ProductsController < ApplicationController
  layout 'dashboard'
  before_action :set_product, only: [:edit, :update, :destroy]

  
  def index
  	respond_to do |format|
  	    format.html
  	    format.json { render json: ProductDatatable.new(view_context) }
  	end
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:success] = "Product has been created!"
      redirect_to products_path
    else
      render "new"
    end
  end

  def edit
    
  end

  def update
    if @product.update_attributes(product_params)
      flash[:success] = "Product has been updated!"
      redirect_to products_path
    else
      render 'edit'
    end
  end

  def destroy
    @product.destroy
    flash[:warning] = "Product has been deleted!"
    redirect_to products_path
  end

  private
    def product_params
      params.require(:product).permit(:product_code, :name, :category_id, :unit_id, :brand_id, :unit_price, :notify_limit)
    end

    def set_product
      @product = Product.friendly.find(params[:id])
    end
end
