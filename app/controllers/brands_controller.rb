class BrandsController < ApplicationController
	def create
		@brand = Brand.create(brand_params)
	end

	def edit
		@brand = Brand.find(params[:id])
	end

	def update
		@brand = Brand.find(params[:id])
		@brand.update_attributes(brand_params)
	end

	def destroy
		@brand = Brand.find(params[:id])
		@brand.destroy
	end


	private
		def brand_params
			params.require(:brand).permit(:name)
		end
end
