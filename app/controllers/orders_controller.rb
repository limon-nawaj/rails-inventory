class OrdersController < ApplicationController

	def index
		@orders = Order.all
	end

	def update
		current_order.update(confirmed: true, total: current_order.subtotal)
		session[:order_id] = nil
		redirect_to cart_path
	end

	def destroy
		@items = current_order.order_items
		@items.each do |item|
			item.destroy
		end
		current_order.destroy
		session[:order_id] = nil
		redirect_to cart_path
	end
end
