class UnitsController < ApplicationController

	def create
		@unit = Unit.create(unit_params)
	end

	def edit
		@unit = Unit.find(params[:id])
	end

	def update
		@unit = Unit.find(params[:id])
		@unit.update_attributes(unit_params)
	end

	def destroy
		@unit = Unit.find(params[:id])
		@unit.destroy
	end


	private
		def unit_params
			params.require(:unit).permit(:name)
		end

end
