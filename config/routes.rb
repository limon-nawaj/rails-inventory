Rails.application.routes.draw do

  devise_for :users, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout', registration: 'register', sign_up: 'cmon_let_me_in' }
  
  resources :orders, only: [:index, :update, :destroy]
  resource  :cart, only: [:show]
  resources :order_items, only: [:create, :update, :destroy]
  
  resources :categories do
    collection do
      post :rebuild
    end
  end

  resources :units
  resources :brands
  resources :products do
    resources :details
  end
  resources :sales


  
  root to: 'static#home'
  get '/dashboard' => 'static#dashboard'
  get '/setting'=>'static#setting'
  # get '/categories/:category' => 'static#products_by_category', as: 'products_by_category'
  
  
end
